package com.sis.ing.qris;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainService {
    public static String baseUrl = "https://map-adawolfs.c9users.io/";

    protected static String token = null;
    protected static String user = null;
    protected static Boolean loginSuccess = false;
    protected static Boolean qrSuccess = false;
    protected static String errorMessage;
    protected static boolean failedConnection, flag;
    protected static final String TAG_USER = "userId";
    protected static final String TAG_SUCCESS = "success";
    protected static final String TAG = "SERVICE";

    public static void initParameters(){
        token = null;
        errorMessage = null;
        failedConnection = false;
    }

    protected static void setUser(String user) {MainService.user = user;}

    public static Boolean getLoginSuccess(){
        return loginSuccess;
    }

    public static Boolean getQRSuccess(){
        return qrSuccess;
    }

    public static void setQRSuccess(Boolean success){
        qrSuccess = success;
    }

    public static void setLoginSuccess(Boolean success){
        loginSuccess = success;
    }

    public static String getUser() {return user;}

    protected static JSONObject postRequest(String url, Map<String, String> params) {
        try {
            Log.i("MainService", baseUrl + url);
            HttpPost post = new HttpPost(baseUrl + url);

            List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
            for (String key : params.keySet()) {
                urlParameters.add(new BasicNameValuePair(key, params.get(key)));
            }
            if(getUser() != null) {
                urlParameters.add(new BasicNameValuePair(TAG_USER, getUser()));
            }
            post.setEntity(new UrlEncodedFormEntity(urlParameters));

            return JsonResponse.getJsonObject(post);

        } catch (Exception e){
            Log.e(TAG, e.getMessage());
            System.err.print(e);
        }
        return null;
    }

    public static void login(String name, String password) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("name", name);
        params.put("password", password);
        JSONObject json = postRequest("loginApp", params);
        try {
            setUser(json.getString(TAG_USER));
            setLoginSuccess(json.getBoolean(TAG_SUCCESS));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void sendQR(String qr){
        setLoginSuccess(Boolean.FALSE);
        Map<String, String> params = new HashMap<String, String>();
        params.put("qr", qr);
        Log.i(TAG, "before send");
        Log.i(TAG, getUser());
        params.put("userId", getUser());
        JSONObject json = postRequest("registerQR", params);
        try {
            Log.i("QR", json.toString());
            setQRSuccess(json.getBoolean(TAG_SUCCESS));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

